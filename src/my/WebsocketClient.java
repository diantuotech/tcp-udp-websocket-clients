package my;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;

abstract class WebsocketClient extends WebSocketClient {
    public WebsocketClient(URI serverURI) {
        super(serverURI);
    }
    @Override
    public void onOpen(ServerHandshake arg0) {
        MyFrame.appendText("handshake success");
    }

    @Override
    public void onClose(int arg0, String arg1, boolean arg2) {
        MyFrame.appendText("Websocket connection closed");
    }

    @Override
    public void onError(Exception arg0) {
        MyFrame.appendText(arg0.getMessage());
    }

    @Override
    public void onMessage(String arg0) {
        MyFrame.appendText("WebsocketServer：" + arg0 + "\n");
    }

}
