package my;

import javax.swing.*;
import java.awt.*;

public class SwingDemo {
    private static void createGUI() {
        MyFrame frame = new MyFrame("TCP/UDP/Websocket Client");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.setSize(700, 400);
        frame.setLocationRelativeTo(null);
        frame.setResizable(false);
        Image icon = Toolkit.getDefaultToolkit().getImage("src/my/icon.png");
        frame.setIconImage(icon);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                createGUI();
            }
        });
    }

}
